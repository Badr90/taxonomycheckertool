<?php
/*

Copyright 2016 International Center for Agricultural Research in the Dry Areas(ICARDA)
 
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
*/

class Taxonomy
{
    public $current_taxonomy_id = "";
    public $genus = "";
    public $species = "";
    public $subspecies = "";

    public $genus_changed = false;
    public $genus_synonym = "";

    public $species_changed = false;
    public $species_synonym = "";

    public $genus_result = false;
    public $genus_suggestions = array();

    public $species_result = false;
    public $species_suggestions = array();

    public $subspecies_result = false;
    public $subs_suggestions = array();

    public $SPAUTHOR_CHECK = "";
    public $SUBTAUTHOR_CHECK = "";

    public function setCurrentId($id)
    {
        $this->current_taxonomy_id = $id;
    }

    public function getCurrentId()
    {
        return $this->current_taxonomy_id;
    }

    public function setGenus($genus)
    {
        $this->genus = $genus;
    }

    public function getGenus()
    {
        return $this->genus;
    }

    public function getGenuschanged()
    {
        return $this->genus_changed;
    }

    public function setGenuschanged($bool)
    {
        $this->genus_changed = $bool;
    }

    public function getSpeciesChanged()
    {
        return $this->species_changed;
    }

    public function setSpeciesChanged($bool)
    {
        $this->species_changed = $bool;
    }

    public function getGenusSynonym()
    {
        return $this->genus_synonym;
    }

    public function setGenusSynonym($syn)
    {
        $this->genus_synonym = $syn;
    }

    public function getGenusResult()
    {
        return $this->genus_result;
    }

    public function setGenusResult($syn)
    {
        $this->genus_result = $syn;
    }

    public function getSpeciesResult()
    {
        return $this->species_result;
    }

    public function setSpeciesResult($res)
    {
        $this->species_result = $res;
    }

    public function getSubspeciesResult()
    {
        return $this->subspecies_result;
    }

    public function setSubspeciesResult($res)
    {
        $this->subspecies_result = $res;
    }

    public function getSpeciesSynonym()
    {
        return $this->species_synonym;
    }

    public function setSpeciesSynonym($syn)
    {
        $this->species_synonym = $syn;
    }

    public function setSpecies($species)
    {
        $this->species = $species;
    }

    public function getSpecies()
    {
        return $this->species;
    }

    public function setSubspecies($Subspecies)
    {
        $this->subspecies = $Subspecies;
    }

    public function getSubspecies()
    {
        return $this->subspecies;
    }

    public function find_synonyms($con)
    {
        $taxonomy_species_id = "";
        $subspecies = $this->getSubspecies();

        //get the taxonomy name
        $taxonomy = $this->getGenus();
        $taxonomy .= ' '.$this->getSpecies();
        if ($subspecies != "") {
            $taxonomy .= ' '.$subspecies;
        }
        //look for the CURRENT taxonomy species id
        $sql = "SELECT current_taxonomy_species_id,taxonomy_species_id FROM `taxonomy_species` where name='$taxonomy'";

        $result1 = mysqli_query($con, $sql);

        //test if different taxonomies with same name
        if ((mysqli_num_rows($result1) > 1) || (mysqli_num_rows($result1) == 0)) {
            return;
        }
        //if unique taxonomy name
        if (mysqli_num_rows($result1) == 1) {
            if ($row1 = mysqli_fetch_row($result1)) {
                $current_taxonomy_species_id = $row1[0];
                $taxonomy_species_id = $row1[1];
            }
        }
		//if taxonomy id has changed find the synonyms
        if ($current_taxonomy_species_id != $taxonomy_species_id) {
            $this->setCurrentId($current_taxonomy_species_id);
            //verify if the fetched species is still having same name
            $sql2 = "SELECT species_name,taxonomy_genus_id FROM `taxonomy_species` where taxonomy_species_id='$current_taxonomy_species_id' "; // the taxonomy_species_id from previous query
            //compare to species in file
            $result2 = mysqli_query($con, $sql2);
            if (mysqli_num_rows($result2) > 0)// found in data base
            {
                if ($row1 = mysqli_fetch_row($result2)) {
                    if (strcasecmp($this->getSpecies(), $row1[0]) != 0) {
                        $this->setSpeciesSynonym($row1[0]);
                        $this->setSpeciesChanged(true);
                    }
                    //keep the genus id
                    $taxonomy_genus_id = $row1[1];
                }
            }

            if ($taxonomy_genus_id != "") {
                // get the genus name using the taxonomy_genus_id we fetched
                $sql3 = "SELECT genus_name FROM `taxonomy_genus` where taxonomy_genus_id='$taxonomy_genus_id' ";
                $result3 = mysqli_query($con, $sql3);
                if (mysqli_num_rows($result3) > 0)// found in data base
                {
                    if ($row1 = mysqli_fetch_row($result3)) {
                        //
                        if (strcasecmp($this->getGenus(), $row1[0]) != 0) {
                            //echo "genus changed<br>";
                            $this->setGenusSynonym($row1[0]);
                            $this->setGenusChanged(true);
                        }
                    }
                }

            }
        } else {
            $this->setCurrentId($taxonomy_species_id);
        }
    }


    public function fetch_authors($con)
    {
		//get current toxonmy id
        $taxonomy_species_id = $this->getCurrentId();
		//select the authorities:
        $query = "Select SPECIES_AUTHORITY, SUBSPECIES_AUTHORITY from taxonomy_species where taxonomy_species_id=$taxonomy_species_id ";
        if ($result = mysqli_query($con, $query)) {
            // Fetch row
            $row = mysqli_fetch_row($result);
            $this->SPAUTHOR_CHECK = $con->real_escape_string($row[0]);
            $this->SUBTAUTHOR_CHECK = $con->real_escape_string($row[1]);
			//add the SYN prefixe :
            if (($this->getGenuschanged() == true) || ($this->getSpecieschanged() == true)) {
                if ($this->SPAUTHOR_CHECK != "") {
                    $this->SPAUTHOR_CHECK = '[Syn] '.$this->SPAUTHOR_CHECK;
                }
                if (($this->SUBTAUTHOR_CHECK != "") && ($this->SUBTAUTHOR_CHECK != " ")) {
                    $this->SUBTAUTHOR_CHECK = '[Syn] '.$this->SUBTAUTHOR_CHECK;
                }
            }
        }
    }


    function subspecies_check($con)
    {
        $species = ltrim($this->species);
        $genus = $this->genus;
        $subspecies = $this->subspecies;
        $filtered_subspecies = "";
		//set the maximum distance based on length 
        $word_length = strlen($subspecies);
        $max_distance = 3;
        if ($word_length > 10) {
            $max_distance = 5;
        }

        $correct = "false";
        $count = 0;

        //filter subspecies from prefixes || only subtaxa with (ssp. or subsp.) prefixes is considered as subspecies
        if (strpos(strtolower($subspecies), 'subsp. ') !== false) {
            $subspecies = str_ireplace('subsp. ', '', $subspecies);

        } else {
            if (strpos(strtolower($subspecies), 'subsp.') !== false) {
                $subspecies = str_ireplace('subsp.', '', $subspecies);
            } else {
                if (strpos(strtolower($subspecies), 'ssp. ') !== false) {
                    $subspecies = str_ireplace('ssp. ', '', $subspecies);
                } else {
                    if (strpos(strtolower($subspecies), 'ssp.') !== false) {
                        $subspecies = str_ireplace('ssp.', '', $subspecies);
                        $filtered_subspecies = "";
                        $subspecies = $filtered_subspecies;
                    } else {
                        return;
                    }
                }
            }
        }
		//filter the subspecies
        for ($c = 0; $c < strlen($subspecies); $c++) {
            if ($subspecies[$c] == " ") {
                break;
            }

            if ($subspecies[$c] == ",") {
                break;
            }

            if ($subspecies[$c] == ".") {
                break;
            }

            $filtered_subspecies = $filtered_subspecies.$subspecies[$c];
        }
        $subspecies = $filtered_subspecies;

        //check if subspecies exists in db
        $sql = "SELECT Taxonomy_species_id, Taxonomy_genus_id from taxonomy_species join taxonomy_genus using(Taxonomy_genus_id) where SUBSPECIES_NAME='$subspecies' and SPECIES_NAME='$species' and GENUS_NAME='$genus'";

        if ($result1 = mysqli_query($con, $sql)) {
            if (mysqli_num_rows($result1) > 0)// found in data base
            {
                $this->setSubspeciesResult("Correct");
                return;
            }
        }

        //check if exists within other taxonomy
        $sql = "SELECT Taxonomy_species_id, Taxonomy_genus_id from taxonomy_species where SUBSPECIES_NAME='$subspecies'";

        if ($result1 = mysqli_query($con, $sql)) {
			//found within other taxonomy
            if (mysqli_num_rows($result1) > 0)
            {
                $correct = "genusandspeciesnotcorresponding";
                $this->setSubspeciesResult(" ");
            }
        }

        $max_distance_counter = 1;

        //look for possible subspecies values
        $sql = "SELECT DISTINCT SUBSPECIES_NAME from taxonomy_species join taxonomy_genus using(Taxonomy_genus_id) where GENUS_NAME='$genus' and SPECIES_NAME='$species'";
        $result1 = mysqli_query($con, $sql);

        if ($result1) {
            if (mysqli_num_rows($result1) > 0)// found in data base
            {
                $genus_species = array();
                //fetch genus and species 's subspecies
                while ($row1 = mysqli_fetch_row($result1)) {
                    $genus_species[] = $row1[0];
                }
                //look for best 3 candidates starting with distance =1,2,3..
                while ($max_distance_counter <= $max_distance) {
                    if ($count == 3) {
                        break;
                    }

                    foreach ($genus_species as $candidate) {

                        $levenshtein = levenshtein($candidate, strtolower($subspecies));
                        if (($levenshtein == $max_distance_counter)) {
							//count number of suggestions
                            $count++;
                            $this->subs_suggestions[] = $candidate;
                            if ($count == 3) {
                                break;
                            }
                        }
						if ($count == 3) {
                            break;
                        }
                    }
                    $max_distance_counter++;
                }

            }

        }
		//if subspecies not found within other taxonomies(genus+species), it is incorrect
        if ($correct != "genusandspeciesnotcorresponding") {
            $this->setSubspeciesResult("Incorrect");
        }
		//if no suggestions found write none
        if ($this->subs_suggestions == null) {
            $this->subs_suggestions[] = "none";
        }

    }


    function species_check($con, $genus)
    {
        $species = ltrim($this->species);
        $genus = $this->genus;
        //echo "genus value $genus";
        //setting distance depending on word length
        $word_length = strlen($species);
        $max_distance = 3;
        if ($word_length > 10) {
            $max_distance = 5;
        }

        $correct = "false";
        $count = 0;

		//check if species exists in the database 
        $sql = "SELECT Taxonomy_species_id, Taxonomy_genus_id from taxonomy_species join taxonomy_genus using(Taxonomy_genus_id) where SPECIES_NAME='$species' and GENUS_NAME='$genus'";
        if ($result1 = mysqli_query($con, $sql)) {
            if (mysqli_num_rows($result1) > 0)// found in data base
            {
                $this->setSpeciesResult("Correct");
                return;
            }
        }

		//check if the species exists within another genus :
        $sql = "SELECT Taxonomy_species_id, Taxonomy_genus_id from taxonomy_species where SPECIES_NAME='$species'";
        if ($result1 = mysqli_query($con, $sql)) {
            if (mysqli_num_rows($result1) > 0)// found within other genus
            {
                $correct = "genusnotcorresponding";
                $this->setSpeciesResult(" ");
            }
        }

        $max_distance_counter = 1;

        //fetch species within same genus
        $sql = "SELECT DISTINCT SPECIES_NAME from taxonomy_species join taxonomy_genus using(Taxonomy_genus_id) where GENUS_NAME='$genus'";
        $result1 = mysqli_query($con, $sql);

        if ($result1) {
            if (mysqli_num_rows($result1) > 0)// found in data base
            {
                $genus_species = array();
                //store species in array
                while ($row1 = mysqli_fetch_row($result1)) {
                    $genus_species[] = $row1[0];
                }
                //look for three best candidates starting with distance =1,2,3..
                while ($max_distance_counter <= $max_distance) {
					//count number of suggestions
                    if ($count == 3) {
                        break;
                    }

                    foreach ($genus_species as $candidate) {
						//calculate the levenshtein distance
                        $levenshtein = levenshtein($candidate, strtolower($species));
                        if (($levenshtein == $max_distance_counter)) //if we have a candidate
                        {
							//count number of suggestions
                            $count++;
                            $this->species_suggestions[] = $candidate;
                            if ($count == 3) {
                                break;
                            }
                        }
						if ($count == 3) {
                            break;
                        }
                    }
					//increment the distance to find more suggestions
                    $max_distance_counter++;
                }

            }

        }
		// if not found within an other genus, then it is incorrect :
        if ($correct != "genusnotcorresponding") {
            $this->setSpeciesResult("Incorrect");
        }
		// if no suggestions found, write none 
        if ($this->species_suggestions == null) {
            $this->species_suggestions[] = "none";
        }
    }

    public function genus_check($genusDB)
    {
        //setting max distance depending on genus length
        $word_length = strlen($this->genus);
        $max_distance = 3;
        if ($word_length > 10) {
            $max_distance = 5;
        }

        $count = 0;

		//check if genus exists in the db
        foreach ($genusDB as $gen) {
            if ((strcasecmp($gen, $this->genus) == 0)) {
                $this->setGenusResult("Correct");
                return;
            }
        }

        $max_distance_counter = 1;

        //look for suggestions while max distance not attended, distance start from 1,2,3 .. up to max_distance
        while ($max_distance_counter <= $max_distance) {
            foreach ($genusDB as $candidate) {
				//count number of suggestions (maximum is 3)
                if ($count == 3) {
                    break;
                }
				//calculate levenshtein distance :
                $levenshtein = levenshtein($candidate, strtolower($this->genus));
                {
					//keep the suggestion if equals distance
                    if (($levenshtein == $max_distance_counter)) 
                    {
                        $this->genus_suggestions[] = $candidate;
                        $count++;
                    }
                }
            }

            if ($count == 3) {
                break;
            }
			//find more suggestions by incrementing the max_distance_counter
            $max_distance_counter++;
        }
		//if no suggestions found write none
        $this->setGenusResult("Incorrect");
        if ($this->genus_suggestions == null) {
            $this->genus_suggestions[] = "none";
        }

    }

}

?>
