<?php
/*
Copyright 2016 International Center for Agricultural Research in the Dry Areas(ICARDA)
 
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
*/

include 'mysql_connection.php';
include 'find_column.php';
include 'class_taxonomy.php';

//check command arguments
if ($argv[1] == "" || $argv[2] == "") {
    echo "Please type input file name, and output file name :\n";
    echo "Example :php check.php INPUTFILENAME.csv OUTPUTFILENAME.csv";
    exit;
}

//get csv input file name 
$target_file = $argv[1];
//set output file name 
$resultfilename = $argv[2];

$imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
$imageFileType2 = pathinfo($resultfilename, PATHINFO_EXTENSION);

// Allow only CSV file formats
if (($imageFileType != "csv") || ($imageFileType2 != "csv")) {
    echo "Sorry, only csv files are accepted \n";
    exit;
}

// Find the MCPD taxonomy columns :
$col = find_column("GENUS", $target_file);
$col2 = find_column("SPECIES", $target_file);
$col3 = find_column("subtaxa", $target_file);

if ($col == -1 || $col2 == -1 || $col3 == -1) {
    echo "Couldn't find columns<br>";
    exit;
}

//open the input file for reading
$handle = fopen($target_file, "r");
//create the output file in append mode
$f = fopen($resultfilename, "a");

$header = array();
$headerLastIndex = 0;
//get the input file header
$header = fgetcsv($handle);

$newHeader = "";

//save the input file header, and last column position
foreach ($header as $column) {
    $newHeader = $newHeader.$column.",";
    $headerLastIndex++;
}

//set the new header :
$newHeader = $newHeader."genus_result, genus_suggestions, sp_result, sp_suggestions, subsp_result, subs_suggestions, SPAUTHOR_CHECK, SUBTAUTHOR_CHECK";
$newHeader .= "\n";

//Write header to output file 
$arr = file($resultfilename);
// edit first line
$arr[0] = $newHeader;
// write the new header to output file
file_put_contents($resultfilename, implode($arr));


//echo "Loading genus data..\n";
$genusDB = array();
$sql = "SELECT genus_name FROM  taxonomy_genus ";
if ($result = mysqli_query($con, $sql)) {
    while ($row = mysqli_fetch_row($result)) {
        $genusDB[] = strtolower($row[0]);
    }
    // Free result set
    mysqli_free_result($result);
}

// make array unique ,in order to avoid duplicated genus data during the check
$genusDB = array_unique($genusDB);

//start reading input file :
//escape reading the header :
if (($fread = fopen($target_file, "r")) !== false) {
    $header = fgetcsv($fread, 1000, ",");
}

while (($record = fgetcsv($fread, 1000, ",")) !== false) 
	{
    if ($record[$col2] != "" && $record[$col] != "") {
		
		//create taxonomy object
        $taxonomy = new Taxonomy;
        $taxonomy->setGenus($record[$col]);
        $taxonomy->setSpecies($record[$col2]);
        $taxonomy->setSubspecies($record[$col3]);

        //find if the genus and species have synonyms
        $taxonomy->find_synonyms($con);

        //genus checking : if no synonyms found for genus call check function:
        if ($taxonomy->getGenuschanged() == false) {
            $taxonomy->genus_check($genusDB);
        }

        $species = "";
        $species_sugg = "";
		
        //crossed species checking
        if ((strpos(strtolower($taxonomy->getSpecies()), ' x ') !== false)) {

			//splitting crossed species
			//get the first crossed species (crossed_species1 x crossed_species2)
            $crossed_species = str_ireplace('x', '', $taxonomy->getSpecies());
            $filtered_species = "";
            for ($c = 0; $c < strlen($crossed_species); $c++) {
                if ($crossed_species[$c] == " ") {
                    break;
                }
                $filtered_species = $filtered_species.$crossed_species[$c];
            }
			//crossed species 1
            $crossed_species1 = $filtered_species;

			//crossed species 2
            $filtered_species = "";
            for ($d = $c; $d < strlen($crossed_species); $d++) {
                $filtered_species = $filtered_species.$crossed_species[$d];
            }
            $crossed_species2 = ltrim($filtered_species);

			//check crossed species 1
            if (($crossed_species1 != '')) {
                $taxonomy1 = new Taxonomy;
                $taxonomy1->setGenus($taxonomy->getGenus());
                $taxonomy1->setSpecies($crossed_species1);
                $taxonomy1->setSubspecies($taxonomy->getSubspecies());
					
                $taxonomy1->species_check($con, $taxonomy1->getGenus());

            }
			//check crossed species2
            if (($crossed_species2 != '')) {
                $taxonomy2 = new Taxonomy;
                $taxonomy2->setGenus($taxonomy->getGenus());
                $taxonomy2->setSpecies($crossed_species2);
                $taxonomy2->setSubspecies($taxonomy->getSubspecies());
				
                $taxonomy2->species_check($con, $taxonomy2->getGenus());
            }
			//get species result correct/incorrect :
            $species = $taxonomy1->species_result;
          
            $species .= " x ";
			//get species result correct/incorrect :        
            $species .= $taxonomy2->species_result;
           
			
			//get suggestions if exist
            if (($taxonomy1->species_suggestions == null) && ($taxonomy2->species_suggestions == null)) {
                $species_sugg = "";
            } else {
                if ($taxonomy1->species_suggestions != null) {
                    $species_sugg = implode('/', $taxonomy1->species_suggestions);
                }

                $species_sugg .= " x ";

                if ($taxonomy2->species_suggestions != null) //foreach($taxonomy2->species_suggestions as $sugg)
                {
                    $species_sugg .= implode('/', $taxonomy2->species_suggestions);
                }

            }

		//if the species is not a crossed one:		
        } else {// always escape species 'sp.'
            if ($taxonomy->getSpecies() !== "sp.") {
                if ($taxonomy->getSpeciesChanged() == false) {
                    $taxonomy->species_check($con, $taxonomy->getGenus());
                }
            }
        }

        //fetching the spauthor and subtauthor for only correct and synonyms
        if (($taxonomy->genus_result != "Incorrect") && ($taxonomy->species_result != "Incorrect")) {
            $taxonomy->fetch_authors($con);
        }

        $genus = $taxonomy->genus_result;

        $genus_sugg = "";

        //get species synonyms and suggestions results
        if (strpos(strtolower($taxonomy->getSpecies()), ' x ') == false) {
            $species = $taxonomy->species_result;
            if ($taxonomy->species_changed == true) {
                $species = "[SYN] ".$taxonomy->getSpeciesSynonym();
            }
            if ($taxonomy->species_suggestions != null) {
                $species_sugg .= implode('/', $taxonomy->species_suggestions);
            }
        }

		//check subspecies
        if ($taxonomy->getSubspecies() != "") {
            $taxonomy->subspecies_check($con);
        }
        $subspecies = $taxonomy->subspecies_result;

		//get genus synonyms and suggestions result 
        if ($taxonomy->genus_changed == true) {
            $genus = "[SYN] ".$taxonomy->getGenusSynonym();
        }
        if ($taxonomy->genus_suggestions != null) {
            $genus_sugg = implode('/', $taxonomy->genus_suggestions);
        }

		$ssp_sugg = "";
        if ($taxonomy->subs_suggestions != null) {
            $ssp_sugg = implode('/', $taxonomy->subs_suggestions);
        }

		//get the data for each row to be written in output file: 
        $data = array();
        for ($i = 0; $i < $headerLastIndex; $i++) {
            $data[] = $record[$i];
        }
		
		//write all results to csv file
        file_put_contents(
            $resultfilename,
            implode(
                ',',
                $data
            ).",".$genus.",".$genus_sugg.",".$species.",".$species_sugg.",".$subspecies.",".$ssp_sugg.",".$taxonomy->SPAUTHOR_CHECK.",".$taxonomy->SUBTAUTHOR_CHECK."\n",
            FILE_APPEND
        );
    }
}

?>
