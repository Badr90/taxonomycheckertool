**Description:**

Taxonomy name checker is a CLI tool, developed to check and complete taxonomy data by running an input file against GrinTaxonomy database available to download as a [CAB archive file ](http://www.ars-grin.gov/~dbmuke/cgi-bin/gringlobal/1.9.6.2/taxonomy_data.cab).

The input file should be in CSV format and include at least the three of MCPD taxonomy columns: GENUS, SPECIES, SUBTAXA.
Only SUBTAXA data with subsp. or ssp. prefixes is considered as subspecies. 

The output file will be in CSV format, and include new columns with found results:

**genus_result:**

Correct : if found on Grin taxonomy  -  
Incorrect : if not found  -
[SYN ] X : Synonym of genus if found (X is synonym genus value)

**genus_suggestions:**

Closest found values to Incorrect genus/ none : if no suggestions found.

**sp_result:**

Correct : if found on Grin taxonomy -
Incorrect : if not found -
[SYN ] X : Synonym of species if found (X is synonym genus value) -
blank : if found in Grin databse within some other genus.
 	 
**sp_suggestions :** 

Closest found values to Incorrect/left blank species/ none : if no suggestions found.

**subsp_result :**

Correct : if found on Grin taxonomy -
Incorrect : if not found - 
[SYN ] X : Synonym of subspecies if found (X is synonym genus value) -
blank : if found in Grin databse within some other genus and species.

**subs_suggestions:**

Closest found values to Incorrect/left blank subspecies/ none : if no suggestions found.

**SPAUTHOR_CHECK:**

species author, may be prefixed with [SYN] if genus or species have synonyms.

**SUBTAUTHOR_CHECK:**

subspecies author, may be prefixed with [SYN] if genus or species have synonyms.


**Database**

The GrinTaxonomySqlFiles folder contains the used sql tables : taxonomy_genus.sql and taxonomy_species.sql . - 
The database name should be included in the mysql_connection.php file, in addition to the user name and password.

**Tool running :**

To run the tool use the command : php check.php INPUTFILENAME.csv OUTPUTFILENAME.csv

The output file will be automatically created in root folder with the name OUTPUTFILENAME.csv